Instructions :
- Créez un petit projet Python (quelques fonctions simples) sur framagit
- Testez votre code via pytest
- Automatisez les tests via Gitlab CI (+Docker)
- Mettez en place un suivi de métriques (coverage) via Gilab (+externe)
- (Mettez en place votre propre runner Gitlab CI
   Connectez-le à Framagit pour votre projet)
- (Mettez en place un serveur Gitlab sur votre machine)

Ressources :
- Pytest : https://docs.pytest.org/en/latest/
- Gitlab CI (intro) : https://docs.gitlab.com/ee/ci/
- Gitlab CI (syntaxe) : https://docs.gitlab.com/ee/ci/yaml/
- Services externes de coverage :
  - codecov : https://codecov.io/
  - coveralls : https://coveralls.io/
  - codacy : https://www.codacy.com/
