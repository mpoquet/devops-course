[![pipeline status](https://gitlab.com/mpoquet/devops-course/badges/master/pipeline.svg)](https://gitlab.com/mpoquet/devops-course/pipelines)
[![slides](https://img.shields.io/badge/slides-online-green.svg)](https://gitlab.com/mpoquet/devops-course/-/jobs/artifacts/master/raw/slides-out/devops.pdf?job=build)
[![git exercise](https://img.shields.io/badge/git%20exercise-online-green.svg)](https://gitlab.com/mpoquet/devops-course/-/jobs/artifacts/master/raw/exos-out/exo-git.pdf?job=build)
[![project](https://img.shields.io/badge/project-online-green.svg)](https://gitlab.com/mpoquet/devops-course/-/jobs/artifacts/master/raw/project-out/project.pdf?job=build)
