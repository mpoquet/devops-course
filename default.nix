{ pkgs ? import (
    fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.09.tar.gz") {}
}:

let
  jobs = rec {
    slides = pkgs.stdenv.mkDerivation rec {
      name = "devops-slides";
      buildInputs = with pkgs; [
        texlive.combined.scheme-context
        ninja
        inkscape
        vim
      ];
      src = pkgs.lib.sourceByRegex ./slides [
        "build.ninja"
        ".*\.tex"
        "img"
        "img/.*\.pdf"
        "img/.*\.svg"
        "img/.*\.jpg"
        ".*\.sh"
      ];
      buildPhase = ''
        patchShebangs .
        ninja -t clean
        ninja
        ./detect-missing-graphics.sh devops.log
      '';
      installPhase = ''
        mkdir -p ''${out}
        mv devops.pdf ''${out}/
      '';
    };

    exos = pkgs.stdenv.mkDerivation rec {
      name = "devops-exos";
      buildInputs = with pkgs; [
        texlive.combined.scheme-context
        ninja
        inkscape
        vim
      ];
      src = pkgs.lib.sourceByRegex ./exo [
        "build.ninja"
        ".*\.tex"
        "fig"
        "fig/.*\.svg"
      ];
      buildPhase = ''
        ninja -t clean
        ninja
      '';
      installPhase = ''
        mkdir -p ''${out}
        mv exo*.pdf ''${out}/
      '';
    };

    project = pkgs.stdenv.mkDerivation rec {
      name = "devops-project";
      buildInputs = with pkgs; [
        texlive.combined.scheme-context
        ninja
      ];
      src = pkgs.lib.sourceByRegex ./project [
        "build.ninja"
        ".*\.tex"
      ];
      buildPhase = ''
        ninja -t clean
        ninja
      '';
      installPhase = ''
        mkdir -p ''${out}
        mv project.pdf ''${out}/
      '';
    };
  };
in
  jobs
