Instructions :
- Créez un projet d'echo server (algo plus bas) TCP en Python sur Framagit
- Faire une première release (0.1.0) du projet
- Ajouter une feature (e.g., afficher les messages reçus)
- Appliquer un correctif de sécurité (factice) sur master et v0.1.0 -> nouvelles releases
- Packager votre projet et soumettre la dernière release sur PyPI (TEST.pypi.org)
- "Dockerisez" votre programme
- (Générer automatiquement une version Docker de votre programme à chaque commit — s'il passe les tests)
- (Regardez comment publier votre programme sur Docker Hub)

Ressources
- Semantic Versioning: https://semver.org/
- Keep a changelog: https://keepachangelog.com/en/1.0.0/
- Release on Gitlab: https://docs.gitlab.com/ee/user/project/releases/
- Gitflow: https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
- Comment packager sur PyPI : https://packaging.python.org/tutorials/packaging-projects/
- Tuto dockerization python : https://runnable.com/docker/python/dockerize-your-python-application
- Référence Dockerfile : https://docs.docker.com/engine/reference/builder/
- Docker Hub : https://docs.docker.com/docker-hub/

Écouter sur un port TCP
Toujours (tant que le processus est vivant)
  Accepter une nouvelle connexion c
  Tant que c n'est pas fermée (par l'autre côté de la connexion)
    Lire des données reçues sur c et les renvoyer à l'identique sur c
